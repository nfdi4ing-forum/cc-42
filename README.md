# Community-Cluster 42 - Construction Engineering and Architecture

![](/img/cc-42-logo.png)

Discussion in the Context of Community Cluster 42 i.e. Research Area "Construction engineering and architecture"  
[Browse Threads or create a new one](https://git.rwth-aachen.de/nfdi4ing-forum/cc-42/-/issues)
